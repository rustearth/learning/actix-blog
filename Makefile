.PHONY: create_user find_user find_by_id

create_user:
	curl -s -H 'Content-Type: application/json' -X POST http://localhost:8080/users -d '{"username":"Frank"}'

find_user:
	curl -s -H 'Content-Type: application/json' http://localhost:8080/users/find/Frank

find_by_id:
	curl -s -H 'Content-Type: application/json' http://localhost:8080/users/1

create_post:
	curl -s -H 'Content-Type: application/json' -X POST http://localhost:8080/users/2/posts -d '{"title":"Bob is here too","body":"Hello friends, also"}'

publish_post:
	curl -s -H 'Content-Type: application/json' -X POST http://localhost:8080/posts/1/publish

comment_post:
	curl -s -H 'Content-Type: application/json' -X POST http://localhost:8080/posts/1/comments -d '{"user_id":2,"body":"Hi Frank, this is your friend Bob"}'

all_posts:
	curl -s -H 'Content-Type: application/json' http://localhost:8080/posts

user_post:
	curl -s -H 'Content-Type: application/json' http://localhost:8080/users/1/posts

schema:
	diesel migration run