use dotenv::dotenv;
use std::env;

fn main() -> std::io::Result<()> {
    dotenv().ok();
    env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let database_url =
        env::var("DATABASE_URL").expect("Environment Variable DATABASE_URL must be set.");
    let app = actixblog::Blog::new(8080);
    app.run(database_url)
}
